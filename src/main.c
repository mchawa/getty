#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#define INITIAL_ARRAY_SIZE		20
#define ARRAY_INCREMENT_SIZE	10

#define INITIAL_STRING_SIZE		100
#define	STRING_INCREMENT_SIZE	50

#define INITIAL_TYPE_SIZE		50
#define TYPE_INCREMENT_SIZE		25

#define DIRECTORY_COMMAND_LITERAL		"!OutputDir:"
#define DIRECTORY_COMMAND_LITERAL_SIZE	11

#define SVN_COMMAND_LITERAL		"!SVN:"
#define GIT_COMMAND_LITERAL		"!GIT:"
#define SRC_COMMAND_LITERAL_SIZE	5

#define TYPE_SVN	0
#define TYPE_GIT	1

#define CD_CMD		"cd "
#define CD_CMD_SIZE		4

#define MKDIR_CMD	"mkdir "
#define MKDIR_CMD_SIZE	7

#define SVN_CMD		"svn export --depth=files "
#define SVN_CMD_SIZE 26

#define GIT_CMD		"git archive --format=tar --remote="
#define GIT_CMD_SIZE 36

static int vidParseFile(FILE *fileCpy, int *retArrayNumOfElem, char ***retArray, int **typeOfElem);
static char *trimwhitespace(char *str);

int main(int argc, const char * argv[])
{
	FILE *file;
	int numOfElem;
	char **retArray;
	int *typeOfElem;

	char cmd[10000];

	assert(argc == 2);

	file = fopen(argv[1], "r");

	assert(file != NULL);

	if(vidParseFile(file, &numOfElem, &retArray, &typeOfElem) == 0)
	{
		int i;
		int j;

		j = 0;
		for(i = 0; i < numOfElem; i++)
		{
			if(i % 2 == 0)
			{
				strcpy(cmd, MKDIR_CMD);
				strcat(cmd, retArray[i]);
				strcat(cmd, " & ");
				strcat(cmd, CD_CMD);
				strcat(cmd, retArray[i]);
				strcat(cmd, " && ");
			}
			else if(typeOfElem[j] == TYPE_SVN)
			{
				strcat(cmd, SVN_CMD);
				strcat(cmd, retArray[i]);
				printf("%s\n", cmd);
				system(cmd);
			}
			else if(typeOfElem[j] == TYPE_GIT)
			{
				strcat(cmd, GIT_CMD);
				strcat(cmd, retArray[i]);
				strcat(cmd, " | tar -x");
				strcat(cmd, " && @for /d %d in (.\\*) do cd %d && forfiles /P . /M * /C \"cmd /c if @isdir==TRUE rmdir /S /Q @file\"");
				printf("%s%s\n", "CMD: ", cmd);
				system(cmd);
			}
			else
			{
				/* Do Nothing */
			}
		}
	}
	else
	{
		printf("Something went wrong !");
	}
}

static int vidParseFile(FILE *fileCpy, int *retArrayNumOfElem, char ***retArray, int **typeOfElem)
{
	char *pStrLoc;
	int strSizeLoc;
	int charCountLoc;

	char  **retArrayLoc;
	int retArraySizeLoc;
	int retArrayNumOfElemLoc;

	int *typeOfElemLoc;
	int typeOfElemSizeLoc;
	int typeOfElemIndex;

	int currCharLoc;

	int retValue;

	pStrLoc = (char *)malloc(INITIAL_STRING_SIZE * sizeof(char));
	strSizeLoc = INITIAL_STRING_SIZE;
	charCountLoc = 0;

	retArrayLoc = (char **)malloc(INITIAL_ARRAY_SIZE * sizeof(char*));
	retArraySizeLoc = INITIAL_ARRAY_SIZE;
	retArrayNumOfElemLoc = 0;

	typeOfElemLoc = (int *)malloc(INITIAL_TYPE_SIZE * sizeof(int));
	typeOfElemSizeLoc = INITIAL_TYPE_SIZE;
	typeOfElemIndex = 0;

	currCharLoc = fgetc(fileCpy);

	retValue = 0;

	while(currCharLoc != EOF)
	{
		if(charCountLoc >= strSizeLoc)
		{
			pStrLoc = (char *)realloc(pStrLoc, (strSizeLoc + STRING_INCREMENT_SIZE) * sizeof(char));
			strSizeLoc += STRING_INCREMENT_SIZE;
		}
		else
		{
			/* Do Nothing */
		}

		if(currCharLoc == '\n')
		{
			pStrLoc[charCountLoc] = '\0';
			charCountLoc++;

			if(retArrayNumOfElemLoc >= retArraySizeLoc)
			{
				retArrayLoc = (char **)realloc(retArrayLoc, (retArraySizeLoc + ARRAY_INCREMENT_SIZE) * sizeof(char*));
				retArraySizeLoc += ARRAY_INCREMENT_SIZE;

				typeOfElemLoc = (int *)realloc(typeOfElemLoc, (typeOfElemSizeLoc + TYPE_INCREMENT_SIZE) * sizeof(int));
				typeOfElemSizeLoc += TYPE_INCREMENT_SIZE;
			}
			else
			{
				/* Do Nothing */
			}

			if(retArrayNumOfElemLoc % 2 == 0) /* This means it's an even line and should contain output directory */
			{
				if(strncmp(pStrLoc, DIRECTORY_COMMAND_LITERAL, DIRECTORY_COMMAND_LITERAL_SIZE) == 0)
				{
					memmove(pStrLoc, &pStrLoc[DIRECTORY_COMMAND_LITERAL_SIZE], charCountLoc - DIRECTORY_COMMAND_LITERAL_SIZE);
					pStrLoc = trimwhitespace(pStrLoc);
					strSizeLoc = strlen(pStrLoc) + 1;
					pStrLoc = (char *)realloc(pStrLoc, strSizeLoc * sizeof(char));
					retArrayLoc[retArrayNumOfElemLoc] = pStrLoc;
				}
				else
				{
					retValue = 1;
					return retValue;
				}
			}
			else /* This means it's an odd line and should contain SVN/GIT link to get files */
			{
				if(strncmp(pStrLoc, SVN_COMMAND_LITERAL, SRC_COMMAND_LITERAL_SIZE) == 0)
				{
					memmove(pStrLoc, &pStrLoc[SRC_COMMAND_LITERAL_SIZE], charCountLoc - SRC_COMMAND_LITERAL_SIZE);
					pStrLoc = trimwhitespace(pStrLoc);
					strSizeLoc = strlen(pStrLoc) + 1;
					pStrLoc = (char *)realloc(pStrLoc, strSizeLoc * sizeof(char));
					retArrayLoc[retArrayNumOfElemLoc] = pStrLoc;

					typeOfElemLoc[typeOfElemIndex] = TYPE_SVN;
				}
				else if(strncmp(pStrLoc, GIT_COMMAND_LITERAL, SRC_COMMAND_LITERAL_SIZE) == 0)
				{
					memmove(pStrLoc, &pStrLoc[SRC_COMMAND_LITERAL_SIZE], charCountLoc - SRC_COMMAND_LITERAL_SIZE);
					pStrLoc = trimwhitespace(pStrLoc);
					strSizeLoc = strlen(pStrLoc) + 1;
					pStrLoc = (char *)realloc(pStrLoc, strSizeLoc * sizeof(char));
					retArrayLoc[retArrayNumOfElemLoc] = pStrLoc;

					typeOfElemLoc[typeOfElemIndex] = TYPE_GIT;
				}
				else
				{
					retValue = 1;
					return retValue;
				}

				typeOfElemIndex++;
			}

			pStrLoc = (char *)malloc(INITIAL_STRING_SIZE * sizeof(char));
			strSizeLoc = INITIAL_STRING_SIZE;
			charCountLoc = 0;

			retArrayNumOfElemLoc++;
		}
		else
		{
			pStrLoc[charCountLoc] = currCharLoc;
			charCountLoc++;
		}

		currCharLoc = fgetc(fileCpy);
	}

	*retArrayNumOfElem = retArrayNumOfElemLoc;
	*retArray = retArrayLoc;
	*typeOfElem = typeOfElemLoc;

	return retValue;
}

static char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator character
  end[1] = '\0';

  return str;
}
