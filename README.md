# Getty

Getty is a minimalist generic dependency manager. It can be seamlessly integrated into makefiles to facilitate building projects that depends on other files in another SVN/GIT repositories.